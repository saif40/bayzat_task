package com.demo;

import org.junit.Test;

import static org.junit.Assert.*;

public class Problem1Test extends Problem1{

    @Test
    public void initiator_with3() {
        assertEquals(3, Initiator(3).size());
    }

    @Test
    public void initiator_with5() {
        assertEquals(8, Initiator(5).size());
    }
}