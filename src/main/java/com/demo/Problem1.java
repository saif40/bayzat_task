package com.demo;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Collectors;


public class Problem1 {


    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the n");
        List<List<Integer>> checking =  Initiator(sc.nextInt());
        System.out.println("number of ways");
        System.out.println(checking.size());

        System.out.println("ways");
        System.out.println(checking);
    }

    protected static List<List<Integer>> Initiator(int n) {
        List<List<Integer>> result = new ArrayList<>();
        char[] input = new char[n];
        for (int i = 0; i < n; i++) {
            input[i]='?';
        }
        for (int i = 0; i <= input.length; i++) {
            printOutput(input, "", 0, i,result);
        }
        return result.stream().filter(x-> {
            Optional<Integer> sum = x.stream().reduce(Integer::sum);
            if (sum.isPresent() && sum.get() == n)
                return true;
            else
                return false;
        }).collect(Collectors.toList());

    }

    private static void printOutput(char[] input, String c, int pos, int t,List<List<Integer>> result) {
        if (c.length() == t) {
            List<Integer> inner = new ArrayList<>();
            char[] chars = c.toCharArray();
            for (int i = 0; i < chars.length; i++) {
                inner.add(Integer.parseInt(chars[i]+""));
            }
            result.add(inner);
        }
        if (pos < t && input[pos] == '?') {
            printOutput(input, c.concat("2"), pos + 1, t,result);
            printOutput(input, c.concat("1"), pos + 1, t,result);
        } else if (pos < t) {
            printOutput(input, c.concat(String.valueOf(input[pos])), pos + 1, t,result);
        }
    }
}